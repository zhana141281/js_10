const form = document.querySelector('form');
const button = document.getElementById('button_submit'); //get button element in the DOM

let warning_text = document.createElement('span'); //create element for warning text
 warning_text.innerText = 'Нужно ввести одинаковые значения';
 warning_text.style.color = 'red';
 button.before(warning_text);
 warning_text.hidden = true;

let val = []; //array for input values

form.addEventListener('input', function (event) {
    let el = event.target;
    return val[`${el.id}`] = el.value;
});

form.addEventListener('click', function (event) {
    let target = event.target;
    switch (target.tagName) {
        case 'I' :
            if (target.classList[1] === 'fa-eye') {
                target.classList.replace('fa-eye', 'fa-eye-slash');
            } else {
                target.classList.replace('fa-eye-slash', 'fa-eye');
            }
        case 'INPUT' :
            if (target.type === 'password') {
                target.setAttribute('type', 'text');
            } else {
                target.setAttribute('type', 'password');
            }
            break;
        case 'BUTTON':
            event.preventDefault();
            console.log(target.tagName);
            if (val[0] === val[1]) {
                warning_text.hidden = true;
                alert('You are welcome!');
            } else {
                warning_text.hidden = false
            }
    }

});


///////////////////////////for me/////////////////////////////////////////
// const inputElem = document.querySelectorAll('input');
// console.log(inputElem);
// let val = [];
//
// const iconElem = document.querySelectorAll('i');
// console.log(iconElem);
//
// const button = document.getElementById('button_submit'); //get button element in the DOM
//
// let warning_text = document.createElement('span'); //create element for warning text
// warning_text.innerText = 'Нужно ввести одинаковые значения';
// warning_text.style.color = 'red';
// button.before(warning_text);
// warning_text.hidden = true;
//
// let res;
// for (let i = 0; i < inputElem.length; i++) {
//     inputElem[i].oninput = function () {
//         val[i] = inputElem[i].value;
//         return val;
//     }
// }
// for (let i = 0; i < iconElem.length; i++) {
//     iconElem[i].onclick = function () {
//         if (iconElem[i].classList[1] === 'fa-eye') {
//             iconElem[i].classList.replace('fa-eye', 'fa-eye-slash');
//             inputElem[i].setAttribute('type', 'text');
//         } else {
//             iconElem[i].classList.replace('fa-eye-slash', 'fa-eye');
//             inputElem[i].setAttribute('type', 'password');
//         }
//
//     }
// }
//
// button.onclick = (event) => {
//     console.log(val);
//     event.preventDefault();
//
//     for (let i = 1; i < val.length; i++) {
//         res = val[i] === val[0];
//         console.log(res);
//     }
//
//     if (res) {
//         warning_text.hidden = true;
//         alert('You are welcome!');
//     }
//
//     else  {warning_text.hidden = false}
// }




